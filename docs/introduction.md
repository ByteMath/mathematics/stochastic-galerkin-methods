# Prime to Stochastic Galerkin Methods

These notebooks aims to provide some additional material to the presentation for the master student colloquium at the University of Groningen. The entire material is based upon the Prime of {cite:p}`constantine2007primer` and the python library  *chaospy* documentation {cite:p}`chaospy`. 

## Goals

* Gain familiarity with the difference between stochastic and deterministic methods
* Learn the main idea of the stochastic Galerkin methods


## Chapters

This book is divided into 3 main components:

* Introduction
* Mathematics background
* Getting started

In the Introduction, we delve into the main idea of the stochastic Galerkin methods and it difference between the classic Galerkin method by means of an example. This section is presentation.

The Mathematics Background section covers the main concepts in more detailed. 

In the final chapter, I provide a brief overview of the instructions on how to locally run this book and its components.

## Stochastic Galerkin Method

Differential equations serve as the foundation for modeling various systems across engineering, physics, biology, chemistry, and environmental sciences. These systems are influenced by uncertainties in initial conditions, boundary conditions, model coefficients, forcing terms, and geometry. It's crucial to thoroughly trace the effects of such uncertainties throughout the system to accurately predict model outputs.

### Polynomial Chaos

Polynomial Chaos (PC) expansions offer an efficient method for representing random processes within stochastic differential equations, aiding in quantifying uncertainty. Stochastic Galerkin (SG) methods, relying on PC expansions, possess several advantages over traditional uncertainty quantification techniques. SG methods demonstrate significantly faster convergence rates compared to conventional Monte Carlo methods. Moreover, unlike perturbation methods and second-moment analysis, SG can handle highly nonlinear systems with substantial uncertainties in random inputs.

However, employing SG entails solving a system of coupled equations, demanding efficient and robust solvers as well as adaptations to existing deterministic code. To address this challenge, a non-intrusive approach known as Stochastic Collocation (SC) comes into play. SC methods leverage interpolation techniques to project a series of deterministic simulations, based on carefully selected parameter sets, onto a polynomial basis. This strategy proves particularly valuable for assessing uncertainty in models implemented with intricate deterministic code that's difficult to modify. Much like SG methods, SC methods exhibit faster convergence rates compared to Monte Carlo methods.

This numerical technique extends the classical Galerkin method to solve stochastic differential equations (SDEs). It incorporates randomness into models, providing more realistic predictions.


## Authors

* Lorenzo Zambelli [website](https://lorenzozambelli.it)
