# Stochastic? Galerkin?

```{figure} ../videos/GalerkinProjection.mp4
    :width: 64%
    :align: left
    :figclass: margin-caption

Galerkin projection vs Stochastic Galerkin Projection - Summary (Click on it to visualize it).
```

Rarely do we encounter systems with deterministic inputs in real-world applications. Over the past decade, the scientific computing community has increasingly turned its attention to stochastic Galerkin schemes (SGS) to address this challenge. These schemes are invaluable when dealing with problems where deterministic models fall short in capturing intrinsic uncertainties.


Consider this scenario: you have a deterministic model for a differential equation and can approximate its solution using standard numerical techniques. However, seeking to enhance the model’s accuracy, you introduce stochastic elements to represent uncertainties inherent in the problem. These uncertainties could stem from various sources, such as measurement errors in equation parameters, randomly perturbed initial or boundary conditions, or the inherent variability in the problem’s domain.


Introducing randomness to the model’s inputs inevitably renders its solution stochastic as well. Effectively, this adds a new dimension of uncertainty to the problem, necessitating a probabilistic framework to accommodate it. So, how do we adapt conventional numerical methods to handle this newfound randomness? How can we modify these techniques to propagate input uncertainties and quantify their impact on the solution? Enter stochastic Galerkin schemes, offering a promising avenue for addressing these questions.


## Why “stochastic Galerkin”?

Stochastic Galerkin (SG) methods employ PC expansions to represent the solution and inputs to stochastic differential equations. A Galerkin projection is used to minimise the error of the truncated expansion and the resulting set of coupled equations can be solved to obtain the expansion coefficients. SG methods are highly suited to dealing with ordinary and partial differential equations and have the ability to deal with steep non-linear dependence of the solution on random model data. Provided sufficient smoothness conditions are met, PC estimates of uncertainty converge exponentially with the order of the expansion and, for low dimensions, come with a small computational cost.

To illustrate the significance of stochastic Galerkin methods, let’s delve into a practical example: modeling the spread of a virus. Typically, virus spread is described using deterministic equations such as the SIR model, dividing the population into susceptible, infected, and recovered individuals.