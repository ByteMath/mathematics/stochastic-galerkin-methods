{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stochastic Galerkin\n",
    "\n",
    "```{figure} ../videos/Summary.mp4\n",
    "    :width: 64%\n",
    "    :align: left\n",
    "    :figclass: margin-caption\n",
    "\n",
    "Stochastic Galerkin Methods summary steps\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The implementation of PCE (or more generally the gPC) involves the following seven steps. \n",
    "\n",
    "* (a) Identify the sources of uncertainty in the model in question. \n",
    "* (b) Choose independent random variables with appropriate PDFs to represent these sources of uncertainty \n",
    "* (c) Construct a generalised Polynomial Basis. \n",
    "* (d) Use this basis to construct a gPC expansion of the sources of uncertainty - initial conditions, parameters etc. \n",
    "* (e) Substitute the gPC expansions into the governing equations. \n",
    "* (f) Perform a Galerkin projection to transform the stochastic equation into a set of coupled deterministic equations. \n",
    "* (g) Solve the resulting system of equations with appropriate numerical methods."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem revisited\n",
    "\n",
    "This section uses a easier case of the SIR model ([problem\n",
    "formulation](./sir_model.ipynb)), were we assumed the Susceptible are just the total population without the infected population, and when you are infected you can't go back to healthy:\n",
    "\n",
    "$$\n",
    "   \\frac{d}{dt} I(t,\\omega) = \\left(1 - \\frac{I}{N}\\right)\\beta\\ I(t,\\omega) \\qquad I(0, \\omega) = \\alpha \\qquad t \\in [0, 10]\n",
    "$$\n",
    "\n",
    "Here $\\alpha$ is initial condition and $\\beta$ is the exponential growth\n",
    "rate. \n",
    "\n",
    "They are both unknown parameters which can be described through a\n",
    "joint probability distribution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:36.621214Z",
     "iopub.status.busy": "2021-05-18T10:56:36.620878Z",
     "iopub.status.idle": "2021-05-18T10:56:38.588823Z",
     "shell.execute_reply": "2021-05-18T10:56:38.589079Z"
    }
   },
   "outputs": [],
   "source": [
    "import chaospy\n",
    "\n",
    "beta = chaospy.Uniform(0.95, 1.05)\n",
    "alpha = chaospy.Normal(10, 2)\n",
    "joint = chaospy.J(beta, alpha)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the parameters are positional defined as $\\alpha$ and $\\beta$\n",
    "respectively.\n",
    "\n",
    "Let assume the model it using polynomial chaos expansion (PCE), by writing the\n",
    "solution $I(t,\\omega)$ as the sum:\n",
    "\n",
    "$$\n",
    "   I(t; \\alpha, \\beta) = \\sum_{n=0}^P c_n(t)\\ \\Phi_n(\\alpha, \\beta)\n",
    "$$\n",
    "\n",
    "Here $\\Phi_n$ are orthogonal polynomials and $c_n$ are the deterministic coefficients. We\n",
    "do not know what the latter is yet, but the former we can construct from\n",
    "distribution alone."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.591144Z",
     "iopub.status.busy": "2021-05-18T10:56:38.590881Z",
     "iopub.status.idle": "2021-05-18T10:56:38.635427Z",
     "shell.execute_reply": "2021-05-18T10:56:38.635655Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "polynomial([1.0, q1-10.0, q0-1.0, q1**2-20.0*q1+96.0])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import chaospy\n",
    "\n",
    "polynomial_expansion = chaospy.generate_expansion(3, joint)\n",
    "polynomial_expansion[:4].round(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note again, that the variables are here defined positional. $\\alpha$ and\n",
    "$\\beta$ corresponds to positions 0 and 1, which again corresponds to the\n",
    "polynomial variables `q0` and `q1` respectively.\n",
    "\n",
    "The second step of the method is to fill in the assumed solution into the\n",
    "equations we are trying to solve the following two equations:\n",
    "\n",
    "$$\n",
    "   \\frac{d}{dt} \\sum_{n=0}^P c_n\\ \\Phi_n = \\left(1 - \\frac{\\sum_{n=0}^P c_n \\ \\Phi_n}{N}\\right)\\beta\\ \\sum_{n=0}^P c_n \\ \\Phi_n\\qquad\n",
    "   \\sum_{n=0}^P c_n(0)\\ \\Phi_n = \\alpha\n",
    "$$\n",
    "\n",
    "The third step is to take the inner product of each side of both equations\n",
    "against the polynomial $\\Phi_k$ for $k=0,\\cdots,P$. For the first equation,\n",
    "this will have the following form:\n",
    "\n",
    "$$\n",
    "\\begin{align*}\n",
    "   \\left\\langle \\frac{d}{dt} \\sum_{n=0}^P c_n \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\left(1 - \\frac{\\sum_{n=0}^P c_n \\ \\Phi_n}{N}\\right)\\beta\\ \\sum_{n=0}^P c_n \\ \\Phi_n, \\Phi_k \\right\\rangle \\\\\n",
    "   \\left\\langle \\sum_{n=0}^P c_n(0)\\ \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\alpha, \\Phi_k \\right\\rangle \\\\\n",
    "\\end{align*}\n",
    "$$\n",
    "\n",
    "Let us define the first equation as the main equation, and the latter as the\n",
    "initial condition equation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Main equation\n",
    "\n",
    "We start by simplifying the equation. A lot of collapsing of the sums is\n",
    "possible because of the orthogonality property of the polynomials $\\langle\n",
    "\\Phi_i, \\Phi_j\\rangle$ for $i \\neq j$.\n",
    "\n",
    "$$\n",
    "\\begin{align*}\n",
    "   \\left\\langle \\frac{d}{dt} \\sum_{n=0}^P c_n \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\left(1 - \\frac{\\sum_{n=0}^P c_n \\ \\Phi_n}{N}\\right)\\beta\\ \\sum_{n=0}^P c_n \\ \\Phi_n, \\Phi_k \\right\\rangle \\\\\n",
    "   \\sum_{n=0}^P \\frac{d}{dt} c_n \\left\\langle \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   -\\sum_{n=0}^P\\sum_{i=0}^P c_nc_i \\left\\langle \\frac{\\beta}{N}\\ \\Phi_n\\Phi_i, \\Phi_k \\right\\rangle + \\sum_{n=0}^P c_n \\left\\langle \\beta\\ \\Phi_n, \\Phi_k \\right\\rangle \\\\\n",
    "      \\frac{d}{dt} c_k \\left\\langle \\Phi_k, \\Phi_k \\right\\rangle &=\n",
    "   -\\sum_{n=0}^P\\sum_{i=0}^P c_nc_i \\left\\langle \\frac{\\beta}{N}\\ \\Phi_n\\Phi_i, \\Phi_k \\right\\rangle + \\sum_{n=0}^P c_n \\left\\langle \\beta\\ \\Phi_n, \\Phi_k \\right\\rangle \\\\\n",
    "   \\frac{d}{dt} c_k &= -\\sum_{n=0}^P\\sum_{i=0}^P c_nc_i \\frac{\\left\\langle \\frac{\\beta}{N}\\ \\Phi_n\\Phi_i, \\Phi_k \\right\\rangle}{\n",
    "      \\left\\langle \\Phi_k, \\Phi_k \\right\\rangle\n",
    "   } +\n",
    "   \\sum_{n=0}^P c_n\n",
    "   \\frac{\n",
    "      \\left\\langle \\beta\\ \\Phi_n, \\Phi_k \\right\\rangle\n",
    "   }{\n",
    "      \\left\\langle \\Phi_k, \\Phi_k \\right\\rangle\n",
    "   }\n",
    "\\end{align*}\n",
    "$$\n",
    "\n",
    "This is a set of linear equations. To solve them in practice, we need to\n",
    "formulate the right-hand-side as a function. To start we create variables to\n",
    "deal with the fact that $\\alpha$ and $\\beta$ are part of the equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.637580Z",
     "iopub.status.busy": "2021-05-18T10:56:38.637307Z",
     "iopub.status.idle": "2021-05-18T10:56:38.644573Z",
     "shell.execute_reply": "2021-05-18T10:56:38.644325Z"
    }
   },
   "outputs": [],
   "source": [
    "alpha, beta = chaospy.variable(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As above, these two variables are defined positional to correspond to both\n",
    "the distribution and polynomial.\n",
    "\n",
    "From the simplified equation above, it can be observed that the fraction of\n",
    "expected values doesn't depend on neither $c$ nor $t$, and can therefore be\n",
    "pre-computed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the denominator $\\mathbb E[\\beta\\Phi_n\\Phi_k]$, since there are both $\\Phi_k$\n",
    "and $\\Phi_n$ terms, the full expression can be defined as a two-dimensional\n",
    "tensor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.646490Z",
     "iopub.status.busy": "2021-05-18T10:56:38.646231Z",
     "iopub.status.idle": "2021-05-18T10:56:38.658420Z",
     "shell.execute_reply": "2021-05-18T10:56:38.658142Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(10,), (10, 10)]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "phi_phi = chaospy.outer(\n",
    "    polynomial_expansion, polynomial_expansion)\n",
    "[polynomial_expansion.shape, phi_phi.shape]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This allows us to calculate the full expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.660422Z",
     "iopub.status.busy": "2021-05-18T10:56:38.660161Z",
     "iopub.status.idle": "2021-05-18T10:56:38.835901Z",
     "shell.execute_reply": "2021-05-18T10:56:38.835661Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1.0e-01, 4.0e-01, 8.3e-05],\n",
       "       [4.0e-02, 4.8e-01, 3.3e-05],\n",
       "       [0.0e+00, 0.0e+00, 0.0e+00]])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "e_beta_phi_phi = chaospy.E(beta*phi_phi, joint)\n",
    "e_beta_phi_phi[:3, :3].round(6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the numerator $\\mbox E(\\Phi_k\\Phi_k)$, it is worth noting that these are\n",
    "the square of the norms $\\|\\Phi_k\\|^2$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.837936Z",
     "iopub.status.busy": "2021-05-18T10:56:38.837677Z",
     "iopub.status.idle": "2021-05-18T10:56:38.880238Z",
     "shell.execute_reply": "2021-05-18T10:56:38.880452Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1.00e+00, 4.00e+00, 8.33e-04, 3.20e+01])"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "_, norms = chaospy.generate_expansion(3, joint, retall=True)\n",
    "norms[:4].round(6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having all terms in place, we can create a function for the right-hand-side\n",
    "of the equation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.882537Z",
     "iopub.status.busy": "2021-05-18T10:56:38.882272Z",
     "iopub.status.idle": "2021-05-18T10:56:38.889040Z",
     "shell.execute_reply": "2021-05-18T10:56:38.888753Z"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "def right_hand_side(c, t):\n",
    "    P = len(c)\n",
    "    A = np.sum(c*e_beta_phi_phi, -1)\n",
    "    for n in range(P):\n",
    "        for i in range(P):\n",
    "            product =  polynomial_expansion[n] * polynomial_expansion[i] * beta/N\n",
    "            inner_product = c[n] * c[i] * chaospy.E(product, joint)\n",
    "            A -= inner_product\n",
    "\n",
    "    return A\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initial conditions\n",
    "\n",
    "The equation associated with the initial condition can be reformulated as\n",
    "follows:\n",
    "\n",
    "$$\n",
    "\\begin{align*}\n",
    "   \\left\\langle \\sum_{n=0}^P c_n(0)\\ \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\alpha, \\Phi_k \\right\\rangle \\\\\n",
    "   \\sum_{n=0}^P c_n(0) \\left\\langle \\Phi_n, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\alpha, \\Phi_k \\right\\rangle \\\\\n",
    "   c_k(0) \\left\\langle \\Phi_k, \\Phi_k \\right\\rangle &=\n",
    "   \\left\\langle \\alpha, \\Phi_k \\right\\rangle \\\\\n",
    "   c_k(0) &=\n",
    "   \\frac{\n",
    "      \\left\\langle \\alpha, \\Phi_k \\right\\rangle\n",
    "   }{\n",
    "      \\left\\langle \\Phi_k, \\Phi_k \\right\\rangle\n",
    "   }\n",
    "\\end{align*}\n",
    "$$\n",
    "\n",
    "Using the same logic as for the first equation we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.891057Z",
     "iopub.status.busy": "2021-05-18T10:56:38.890749Z",
     "iopub.status.idle": "2021-05-18T10:56:38.900930Z",
     "shell.execute_reply": "2021-05-18T10:56:38.901150Z"
    }
   },
   "outputs": [],
   "source": [
    "e_alpha_phi = chaospy.E(alpha*polynomial_expansion, joint)\n",
    "initial_condition = e_alpha_phi/norms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Equation solving\n",
    "\n",
    "With the right-hand-side for both the main set of equations and the initial\n",
    "conditions, it should be straight forward to solve the equations numerically.\n",
    "For example using `scipy.integrate.odeint`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.903384Z",
     "iopub.status.busy": "2021-05-18T10:56:38.903124Z",
     "iopub.status.idle": "2021-05-18T10:56:38.911000Z",
     "shell.execute_reply": "2021-05-18T10:56:38.910740Z"
    }
   },
   "outputs": [],
   "source": [
    "from scipy.integrate import odeint\n",
    "\n",
    "coordinates = np.linspace(0, 10, 100)\n",
    "coefficients = odeint(func=right_hand_side,\n",
    "                      y0=initial_condition, t=coordinates)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These coefficients can then be used to construct the approximation for $u$\n",
    "using the assumption about the solutions form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.913220Z",
     "iopub.status.busy": "2021-05-18T10:56:38.912894Z",
     "iopub.status.idle": "2021-05-18T10:56:38.936483Z",
     "shell.execute_reply": "2021-05-18T10:56:38.936234Z"
    }
   },
   "outputs": [],
   "source": [
    "u_approx = chaospy.sum(polynomial_expansion*coefficients, -1)\n",
    "u_approx[:4].round(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, this can be used to calculate statistical properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.938754Z",
     "iopub.status.busy": "2021-05-18T10:56:38.938439Z",
     "iopub.status.idle": "2021-05-18T10:56:38.953843Z",
     "shell.execute_reply": "2021-05-18T10:56:38.954059Z"
    }
   },
   "outputs": [],
   "source": [
    "mean = chaospy.E(u_approx, joint)\n",
    "variance = chaospy.Var(u_approx, joint)\n",
    "\n",
    "mean[:5].round(6), variance[:5].round(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2021-05-18T10:56:38.956492Z",
     "iopub.status.busy": "2021-05-18T10:56:38.956225Z",
     "iopub.status.idle": "2021-05-18T10:56:39.024700Z",
     "shell.execute_reply": "2021-05-18T10:56:39.024917Z"
    }
   },
   "outputs": [],
   "source": [
    "from matplotlib import pyplot\n",
    "\n",
    "pyplot.rc(\"figure\", figsize=[6, 4])\n",
    "\n",
    "pyplot.xlabel(\"coordinates\")\n",
    "pyplot.ylabel(\"model approximation\")\n",
    "pyplot.axis([0, 10, 0, 100])\n",
    "sigma = np.sqrt(variance)\n",
    "pyplot.fill_between(coordinates, mean-sigma, mean+sigma, alpha=0.3)\n",
    "pyplot.plot(coordinates, mean)\n",
    "pyplot.show()"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:percent"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
