# Author: Lorenzo Zambelli
# License: GPL-3.0

# Use the base Jupyter image with scipy latest
FROM jupyter/scipy-notebook:latest

# Arguments and environment variables
ARG NB_USER=lorenzo
ARG NB_UID=8888
ENV NB_USER=${NB_USER}
ENV NB_UID=${NB_UID}
ENV HOME="/home/${NB_USER}"
ENV PATH="${HOME}/.local/bin:${PATH}"

# Switch to root user for setup
USER root
RUN apt-get update && apt-get install -y entr

# Create a non-root user with specified UID and add Python packages
RUN id -u ${NB_USER} &>/dev/null || adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER} && \
    mkdir -p ${HOME}/.local/lib/python3.7 && \
    chown -R ${NB_UID} ${HOME}/.local

# Switch back to non-root user
USER ${NB_USER}

# Upgrade pip
RUN pip install --upgrade pip

# Copy the requirements file to the home directory of the Jupyter user
COPY requirements.txt ${HOME}/

# Upgrade Jupyter installation and install Jupyter Book packages
RUN pip install --no-cache-dir -r ${HOME}/requirements.txt

# Jupyter notebook extensions
RUN pip install jupyter_contrib_nbextensions

# Copy the documentation folder into the container
COPY docs ${HOME}/docs

# Set ownership of the copied folder to the non-root user
USER root
RUN chown -R ${NB_UID} ${HOME}/docs
USER ${NB_USER}

# Set working directory
WORKDIR ${HOME}/docs

# Set the default command to clean and build the Jupyter Book
CMD ["sh", "-c", "jupyter-book clean . && jupyter-book build ."]





