# Stochastic Galerkin Method

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ByteMath%2Fmathematics%2Fstochastic-galerkin-methods/main)

These notebooks aims to provide some additional material to the presentation for the master student colloquium at the University of Groningen. The entire material is based upon the Prime of {cite:p}`constantine2007primer` and the python library  *chaospy* documentation {cite:p}`chaospy`. 

## Goals

* Gain familiarity with the difference between stochastic and deterministic methods
* Learn the main idea of the stochastic Galerkin methods


## Chapters

This book is divided into 3 main components:

* Introduction
* Mathematics background
* Getting started

In the Introduction, we delve into the main idea of the stochastic Galerkin methods and it difference between the classic Galerkin method by means of an example. This section is presentation.

The Mathematics Background section covers the main concepts in more detailed. 

In the final chapter, I provide a brief overview of the instructions on how to locally run this book and its components.


## Installation and Local Setup

To utilize this book locally, you'll need to set up Docker on your machine. Follow the steps below to install Docker:

1. **Install Docker:** Depending on your operating system, you can find the installation instructions on the [official Docker website](https://docs.docker.com/get-docker/).

    - For Windows: [Install Docker Desktop for Windows](https://docs.docker.com/desktop/windows/install/).
    - For macOS: [Install Docker Desktop for Mac](https://docs.docker.com/desktop/mac/install/).
    - For Linux: [Install Docker Engine on Linux](https://docs.docker.com/engine/install/).

2. **Running the Book:**
   
   Once Docker is installed, running the book locally is straightforward. Follow these steps:

   - Navigate to the main directory of the project.
   - Run the following command to start the Docker containers:
     ```
     docker-compose up
     ```
   - Next Get Notebook Server Link (something of this form http://0.0.0.0:8888/?token=bcd90816a041fa1f966829d1d46027e4524f40d97b96b8e0 ) 

   ![](docs/images/docker-compose.png)

   - Then in a new terminal run the following command

```bash
docker-compose exec jupyter bash

``` 

![](docs/images/docker-exc.png)


- Next, execute the following commands to clean the cache and build the book:
     ```
     jb clean . && jb build .
     ```
   - After the book is built successfully, access the local server with the notebook by entering the following URL in your browser:
     ```
     http://127.0.0.1:8080/files/_build/html/index.html
     ```
     or, if it does not work, go to the notebook server 

     ![](docs/images/notebook-server.png)

    and navigate to the `_build/html` directory and click on `index.html` to open the book locally.

By following these steps, you'll be able to install Docker, run the book locally, and explore its contents with ease. Happy learning!

Whenever you want to make changes, re run 
```
     jb clean . && jb build .
     ```
as before and reload the localhost page.


## Authors

* Lorenzo Zambelli [website](https://lorenzozambelli.it)


