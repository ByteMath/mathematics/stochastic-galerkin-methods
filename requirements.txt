# Install Jupyter and related packages
jupyter
jupyter-core
jupyter-console
pyzmq
tornado
traitlets

# Install Jupyter Book
jupyter-book

# Install ghp-import
ghp-import

# Install python packages
matplotlib
numpy
chaospy